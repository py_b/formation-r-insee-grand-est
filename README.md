---
title: "Formation R Insee Grand Est"
---

![](https://assets.gitlab-static.net/uploads/-/system/project/avatar/17842794/images.jpg?width=64)

_____________________________________

## Principe de la formation

Il s'agit d'une autoformation, avec accompagnement.

Chaque module se compose de plusieurs séquences. Pour suivre une séquence, il faut avoir suivi la précédente.

Une séquence se déroule en 2 temps :
 
- les stagiaires inscrits reçoivent un lien vers le support en ligne leur permettant de réaliser seuls la séquence. Ils disposent pour cela d'environ une semaine. Les formateurs restent à leur disposition pour toute question ou difficulté durant cette période.
 - une séance collective de débriefing d'environ 1 heure se tient après chaque séquence.

## Modules actuellement achevés

### Manipulation de tables avec `dplyr` et `tidyr`

Filtrer et trier une table, sélectionner et créer des variables, faire des statistiques descriptives simples,
joindre des tables, transposer une table

- [Séquence 1 : introduction à dplyr](https://py_b.gitlab.io/formation-r-insee-grand-est/manipulation_tables_seq1_introduction_dplyr.html)
- [Séquence 2 : approfondissement de dplyr](https://py_b.gitlab.io/formation-r-insee-grand-est/manipulation_tables_seq2_dplyr_approfondissement.html)
- [Séquence 3 : rapprochement de tables avec dplyr](https://py_b.gitlab.io/formation-r-insee-grand-est/manipulation_tables_seq3_rapprochement.html)
- [Séquence 4 : transposition avec tidyr](https://py_b.gitlab.io/formation-r-insee-grand-est/manipulation_tables_seq4_transposition.html)

### Graphiques avec `ggplot2`

Réaliser des diagrammes en barres, des histogrammes, des courbes, des nuages de points, des boîtes à moustaches,
les mettre en forme et les annoter.

- [Séquence 1 : introduction à ggplot2](https://py_b.gitlab.io/formation-r-insee-grand-est/ggplot2_seq1_introduction.html)
- [Séquence 2 : les options](https://py_b.gitlab.io/formation-r-insee-grand-est/ggplot2_seq2_options.html)
- [Séquence 3 : coordonnées, axes et mise en forme](https://py_b.gitlab.io/formation-r-insee-grand-est/ggplot2_seq3_mise_en_forme.html)

### Programmation avec R

Rendre son code simple, efficace et robuste en programmant avec R.

_Pré-requis : avoir déjà pratiqué R et être familiarisé avec les structures de données les plus courantes (vecteurs, data.frames...)._

- [Séquence 1 : conditions et boucles](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq1_conditions_boucles.html)
- [Séquence 2 : utilisation et création de fonctions](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq2_fonctions.html)
- [Séquence 3 : un peu de programmation fonctionnelle (`lapply` & friends)](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq3_prog_fonctionnelle.html)
- [Séquence bonus : résolution d'un cas concret](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq4_bonus.html)

_____________________________________

[Code source, contribuer](https://gitlab.com/py_b/formation-r-insee-grand-est)
