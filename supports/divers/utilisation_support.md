Ce support contient du code R, présenté dans des blocs de code :

```r
# ceci est un bloc de code
```

Le code contenu dans ces blocs peut être copié, collé et exécuté dans R.
L'intégralité du code utilisé dans ce document a été regroupé en
[annexe](#annexe).

Ce qui s'affiche dans la console de R apparaîtra sous cette forme dans ce
document :

```
## Bonjour, je suis votre console.
```

La solution des exercices est cachée dans des blocs de texte, sur lesquels
il faut cliquer pour les déplier :

<details>
  <summary>Solution</summary>
``` r
# Vous trouverez la solution dans un bloc de ce genre
# Ne pas cliquer immédiatement !
```
</details>
