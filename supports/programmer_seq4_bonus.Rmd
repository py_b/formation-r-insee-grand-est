---
title: "Programmer avec R"
subtitle: "Séquence bonus : résolution d'un cas concret"
output:
  html_document:
    toc: yes
    toc_depth: 2
    toc_float: yes
---

# Préambule

Cette séquence n'aborde pas de nouvelles notions. Elle utilise divers éléments des trois séquences du module pour résoudre un cas concret.

La décomposition en étapes vise par ailleurs à montrer qu'un code élégant et efficace s'obtient la plupart du temps par une suite d'essais et améliorations successives. Peu de gens écrivent un programme parfait du premier coup !

### Utilisation de ce support

Ce support contient du code R, présenté dans des blocs de code :

```r
# ceci est un bloc de code
```

Le code contenu dans ces blocs peut être copié, collé et exécuté dans R.

Ce qui s'affiche dans la console de R apparaîtra sous cette forme dans ce
document :

```
## Bonjour, je suis votre console.
```

________________________________________________________________________________

# Problématique

Un collègue vous donne une liste de _data.frames_ de ce type :

```{r "data1"}
mes_donnees <-
  list(
    data.frame(A = 1:2, B = 3:4),
    data.frame(C = 10),
    data.frame(A = -5, B = 11)
  )

mes_donnees
```

>Il vous demande de produire une liste agrégée en faisant en sorte que les _data.frames_ possédant les mêmes noms de colonnes soient assemblés en un seul _data.frame_.

Par exemple, agréger `mes_donnees` devrait produire la liste de 2 éléments suivante :

```{r "show_res", echo = FALSE}
list(
  data.frame(A = c(1:2, -5), B = c(3:4, 11)),
  data.frame(C = 10)
)  
```

Les éléments initiaux 1 et 3 ont été assemblés, l'élément 2 n'a été assemblé avec rien (il est resté tel quel).

> Votre collègue vous indique qu'une telle liste vous sera fournie régulièrement. Le nombre de _data.frames_ contenus sera variable, ainsi que les noms des colonnes de ceux-ci.
>
> ![](images/warning_s.png)

________________________________________________________________________________

# Manuellement

Vous ne savez pas comment assembler verticalement plusieurs _data.frames_ mais après une courte recherche sur internet, vous découvrez que le package `dplyr` dispose d'une fonction `bind_rows` conçue pour réaliser cette tâche.

**bind_rows**\
![](images/bind_rows.png)

La fonction `bind_rows` est pratique car elle peut agréger des **listes** contenant un ou plusieurs _data.frames_.

Après quelques essais, vous aboutissez au code suivant qui donne un résultat conforme aux attentes :

```{r message = FALSE, warning = FALSE}
library(dplyr) # (pour `bind_rows`)

list(
  bind_rows(mes_donnees[c(1, 3)]),
  bind_rows(mes_donnees[2])
)
```

Cette méthode manuelle fonctionne pour la liste que vous avez actuellement, mais ne marchera pas pour celles qui vont suivre. Il faut donc l'améliorer.

________________________________________________________________________________

# Début d'automatisation

La méthode précédente consiste à regrouper des éléments avant d'appliquer à chaque groupe une fonction qui transforme plusieurs éléments en un seul. Le schéma explicatif de la fonction `tapply` vu dans la [séquence 3](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq3_prog_fonctionnelle.html) (fonctions _apply_) vous revient donc rapidement à l'esprit :

![](images/prog3_2-tapply.png)

`X` sera la liste des _data.frames_ et `FUN` sera `dplyr::bind_rows`. Reste à définir `INDEX`.

Cet argument doit contenir autant d'éléments que `mes_donnees` et chaque élément doit indiquer à quel groupe appartient l'élément de `X` à la même position. Vous tentez de définir ces groupes manuellement :


```{r}
groupes <- c(1, 2, 1)
```

Les premier et troisième _data.frames_ appartiennent au groupe 1 et le deuxième _data.frame_ au groupe 2.

Ces noms sont arbitraires, l'important étant dans le cas présent que le premier élément soit égal au troisième... On pourrait par exemple choisir des noms formés en concaténant les noms de colonnes d'un _data.frame_ :

```{r}
groupes <- c("A_B", "C", "A_B")
```

Ces noms paraissent plus informatifs, vous décidez de les garder. Vous avez maintenant toutes les informations nécessaires pour utiliser `tapply` :

```{r}
tapply(mes_donnees, groupes, bind_rows)
```

Le résultat est correct. Vous avez amélioré quelque peu le processus, mais vous pouvez encore aller plus loin. En effet, si la liste change, il faut toujours adapter manuellement le vecteur des groupes.

________________________________________________________________________________

# Génération automatique des groupes

**Et si le vecteur des groupes était généré automatiquement à partir de la liste des _data.frames_ ?**

Vous essayez sur le premier élément de la liste, auquel vous attribuez le nom `x`, pour simplifier l'écriture.

```{r}
x <- mes_donnees[[1]]
```

Il est possible de récupérer les noms des colonnes...

```{r}
colnames(x)
```

...qu'on peut ensuite concaténer en une chaîne unique.

```{r}
paste(colnames(x), collapse = "_")
```

Ça fonctionne pour cet élément, et aussi si l'on prend le 2e ou 3e élément.

Mais comment faire ça pour tous les _data.frames_ à la fois ? Pour rappel, on voudrait autant de chaînes de caractères en sortie que de _data.frames_ en entrée.

C'est précisément ce que réalise la fonction `lapply` :

![](images/prog3_1-lapply.png)
```{r}
groupes <-
  lapply(
    mes_donnees,
    function(x) paste(colnames(x), collapse = "_")
  )

groupes
```

On a bien les mêmes groupes que ceux définis à la main auparavant. La seule différence est que l'objet produit est une liste au lieu d'un vecteur. Qu'à cela ne tienne, vous connaissez l'existence de variantes simplificatrices : 

```{r}
groupes <-
  sapply(
    mes_donnees,
    function(x) paste(colnames(x),  collapse = "_")
  )

groupes
```

Parfait. Vous injectez cette information dans `tapply()` et, sans suprise, cela fonctionne :

```{r}
tapply(mes_donnees, groupes, bind_rows)
```

> De nouvelles données à agréger vous parviennent. Vous les utilisez pour confirmer la validité du code que vous venez d'écrire :

```{r}
# nouvelles données
mes_donnees_v2 <-
  list(
    data.frame(A = 1:2, B = 3:4),
    data.frame(C = 10),
    data.frame(A = -5, B = 11),
    data.frame(C = 101:103),
    data.frame(B = 243:245, A = -1:1),
    data.frame(E = "salut")
  )

# regénération des groupes (code identique)
groupes <-
  sapply(
    mes_donnees_v2,
    function(x) paste(colnames(x), collapse = "_")
  )

# on agrège (code identique)
tapply(mes_donnees_v2, groupes, bind_rows)
```

Les éléments `C` et `E` sont conformes. En revanche, les _data.frames_ contenant des colonnes `A` et `B` n'ont pas été tous fusionnés à cause de l'ordre différent des colonnes.

Or, pour `bind_rows`, un ordre des colonnes différent n'est pas bloquant. Une solution est de faire en sorte que le nom du groupe soit le même. Pour cela, on peut modifier la fonction anonyme afin qu'elle trie (`sort`) les noms de colonnes avant de les concaténer :

```{r}
groupes <-
  sapply(
    mes_donnees_v2,
    function(x) paste(sort(colnames(x)), collapse = "_") # ajout d'un tri
  )

tapply(mes_donnees_v2, groupes, bind_rows)
```

![](images/congrats.gif)

Vous avez fait le plus dur !

Voyons maintenant comment rendre ce traitement encore plus simple, efficace et robuste.

________________________________________________________________________________

# Création d'une fonction

## Éviter les copier-coller

Le code est fonctionnel, mais si les données changent de noms (`mes_donnees` qui devient `mes_donnees_v2`...), il faut répercuter ce changement à plusieurs endroits (dans `sapply` et dans `tapply`).

Lorsqu'une telle situation apparaît, la solution est de [créer une fonction](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq2_fonctions.html). On lui choisit nom informatif (disons `agrege_identiques`) et on lui attribue un unique paramètre `liste_df` (un nom générique).

Quant au corps de la fonction, il est tout simplement constitué des instructions écrites précédemment (on remplace juste le nom réel des données par le nom générique).

```{r "fonction_v1"}
agrege_identiques <- function(liste_df) {
  
  groupes <-
    sapply(
      liste_df,
      function(x) paste(sort(colnames(x)), collapse = "_")
    )
  
  tapply(liste_df, groupes, bind_rows)

}
```

Une fois cette fonction chargée en mémoire, on n'a plus qu'à l'utiliser en lieu et place de nos deux instructions de départ :

```{r eval = FALSE}
agrege_identiques(mes_donnees)
agrege_identiques(mes_donnees_v2)
```

## Rendre le code plus robuste

Pour peaufiner, on peut contrôler si chaque élément de la liste passée en paramètre est un _data.frame_. On utilise pour cela une condition `if` combinée à `stop` (vus dans la séquence 1 : [conditions et boucles](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq1_conditions_boucles.html)).

On inspecte chaque _data.frame_ avec `sapply(liste_df, is.data.frame)`. Comme `if` n'attend qu'**un seul** `TRUE` ou `FALSE` en condition, on synthétise le résultat avec `all()`.

On utilise `vapply` au lieu de `sapply` dans la condition car, comme on l'a vu dans la séquence 3, cette fonction est plus sûre et rapide (on fait également ce remplacement par `vapply` pour la création des groupes).

On obtient ainsi une **version finale de la fonction** qui répond à la problématique de départ :

```{r "fonction_finale"}
agrege_identiques <- function(liste_df) {
  
  if (!all(vapply(liste_df, is.data.frame, logical(1)))) {
    stop("certains éléments ne sont pas des data.frames !")
  }
  
  groupes <-
    vapply(
      liste_df,
      function(x) paste(sort(colnames(x)), collapse = "_"),
      FUN.VALUE = character(1)
    )
  
  tapply(liste_df, groupes, bind_rows)

}
```

Testons ces ultimes ajouts sur les nouvelles données suivantes :

```{r}
mes_donnees_v3 <-
  list(
    data.frame(TEXTE = letters[1:4]),
    data.frame(A = 1:2, B = 3:4),
    data.frame(C = 10),
    data.frame(A = -5, B = 11),
    data.frame(C = 101:103),
    data.frame(B = 243:245, A = -1:1),
    data.frame(TEXTE = "salut")
  )

mes_donnees_v4 <-
  list(
    data.frame(A = 1:2, B = 3:4),
    data.frame(A = -5, B = 11),
    data.frame(C = 101:103),
    "JE NE SUIS PAS UN DATA.FRAME !!!!!",
    data.frame(E = "salut")
  )
```

```{r}
agrege_identiques(mes_donnees_v3)
```

```{r error = TRUE}
agrege_identiques(mes_donnees_v4)
```

________________________________________________________________________________

# Pour les perfectionnistes

On pourrait pousser encore le raffinement de cette fonction, par exemple :

- signaler *quels éléments* de la liste ne sont pas des_ data.frames_ ;
- donner la possibilité à l'utilisateur d'ignorer les éléments problématiques ;
- gérer mieux les concaténations de noms de variables. Par exemple, que se passe-t-il si un _data.frame_ contient une colonne `"A_B"` ?
- ...

Ces développements sont laissés en exercice au lecteur.

________________________________________________________________________________

# Références

Les 3 séquences du module d'autoformation "Programmer avec R" :

- [Séquence 1 : conditions et boucles](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq1_conditions_boucles.html)
- [Séquence 2 : utilisation et création de fonctions](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq2_fonctions.html)
- [Séquence 3 : un peu de programmation fonctionnelle](https://py_b.gitlab.io/formation-r-insee-grand-est/programmer_seq3_prog_fonctionnelle.html)
